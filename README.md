# lec_2019_citu
# team dh thai nguyen
# project: model oto tracking and following object

/****** Doccument for use this source code ******/

* Setting environment for run code:

  1. Install Opencv with tutorial in below link:
https://www.pyimagesearch.com/2018/05/28/ubuntu-18-04-how-to-install-opencv/?fbclid=IwAR1XNIFyOpdoOkORoyCD4MEhCxU1tO-TYUP_AoS8zHT7_jxq59kepBYGJXo

  2. install VNC on your laptop to remote control Udoo-neo.

* After have been done two step above, follow to these step below:
  Following step are working on source code in the folder Final_Milestone.
----------------------------------------------------------------------------
step1: - Connect your Udoo Neo board with Laptop through USB 3.0 port.
       - And use VNC to do these next step.

step2: - Open file corem4.ino in folder lec_2019_ictu/coreM4/ by Arduino-IDE on 
         the desktop of Udoo neo.
       - Click to upload button at the top-left of Arduino IDE to upload code for core-M4.

step3: - Open terminal on Udoo-neo screen, cd to folder source code.
         it's folder: lec_2019_ictu/Final_Milestone/
       - run following command to run code Python for Image Process:
	  + python3 imageProcess.py

step4: - Plug-out the cab connect Udoo-neo with Laptop.
       - Use the ball to control direct of your car.


